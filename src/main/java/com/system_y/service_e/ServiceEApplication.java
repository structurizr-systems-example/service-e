package com.system_y.service_e;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServiceEApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceEApplication.class, args);
	}
}
